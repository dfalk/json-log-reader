var React = require('react')
var TextFieldEntry = require('./TextFieldEntry');

module.exports = React.createClass({
  getInitialState: function() {
    return JSON.parse(window.localStorage.getItem(this.props.storageKey)) || {};
  },
  groupValuesChange: function (newValues) {
    this.setState({groupBy: newValues});
  },
  saveSettings: function () {
    window.localStorage.setItem(this.props.storageKey, JSON.stringify(this.state));
    this.props.onClose(true);
  },
  render: function () {
    return React.createElement('div', null, [
      React.createElement('h1', {key: 'h1' }, 'Settings'),
      React.createElement(TextFieldEntry, {onChange: this.groupValuesChange, values: this.state.groupBy, key: 'groups' }),
      React.createElement('div', { className: 'buttons', key: 'buttons' }, [
        React.createElement('button', {onClick: this.saveSettings, key: 'save'}, 'Apply Settings'),
        React.createElement('button', {onClick: () => this.props.onClose(false), key: 'cancel'}, 'Cancel')
      ])
    ]);
  }
});
