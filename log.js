'use strict';

const React = require('react');
const ReactDOM = require('react-dom');
const Tree = require('./TreeComponent');
const Settings = require('./Settings');
const json = require('json5');
const Inspector = require('react-json-inspector');

let parsedEntries = []; // The array of lines that are parsed from json
let organizedEntries;  // The data structure that will hold grouped entries
let logDiv;  // Maintain a reference to the log container

// Settings variables
let groupByProperties;

// Need these to make Resizable required library work
global.window.cancelAnimationFrame = function (id) {
  return window.cancelAnimationFrame(id);
};
global.window.requestAnimationFrame = function (fn) {
  return window.requestAnimationFrame(fn);
};

function ready(fn) {
  if (document.readyState != 'loading'){
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}

ready(function() {
  applyConfig();

  document.getElementById('filename').innerHTML = global.file;
  logDiv = document.getElementById('logContainer');

  Split(['#logContainer', '#logDetailContainer'], {
    sizes: [25, 75],
    minSize: 200
  });


  var Tail = require('always-tail');
  var tail = new Tail(global.file, '\n', {
    start: 0
  });

  tail.on('line', function (data) {
    if (!data.length) {
      return;
    }
    let entry;
    try {
      entry = json.parse(data);
    }
    catch (e) {
      console.warn("Couldn't parse line:", data, e);
      return;
    }

    parsedEntries.push(entry);
    catalog(entry);
    renderLog();
  });

  tail.watch();
});

// catalog takes a parsed log entry object and puts it into the
// organizedEntries, which backs the log tree component
function catalog(data) {
  var currentGroup = organizedEntries, len = groupByProperties.length;
  for (let i = 0; i <= len; ++i) {
    if (i === len) {
      currentGroup.push({label: json.stringify(data), data});
      continue;
    }
    var label = data[groupByProperties[i]];
    if (!label) {
      currentGroup.set({}, {label: json.stringify(data), data});
      break;
    }

    if (currentGroup.has(label)) {
      currentGroup = currentGroup.get(label).children;
      continue;
    }

    if (i === len - 1) {  // The last one
      children = [];
    }
    else {
      var children = new Map();
    }
    currentGroup.set(label, { label, children, data });
    currentGroup = children;
  }
}

// Turn the config string from local storage into variables
function applyConfig() {
  groupByProperties = window.localStorage.getItem(global.file);
  groupByProperties = (groupByProperties && JSON.parse(groupByProperties).groupBy) || [];
  organizedEntries = groupByProperties.length ? new Map() : [];
}

function onItemSelected(item) {
  function copyToClipboard() {
    var gui = require('nw.gui');
    var clipboard = gui.Clipboard.get();
    clipboard.set(JSON.stringify(item.data))
  }
  let element = React.createElement('div', null, [
    React.createElement('a', {className: 'copy', href: '#', onClick: copyToClipboard}, 'copy to clipboard'),
    React.createElement(Inspector, {data: item.data, isExpanded: () => true})
  ]);
  ReactDOM.render(element, document.getElementById('logDetailContainer'))
}

// (Re-)render the log
function renderLog() {
  let element = React.createElement(Tree, { data: organizedEntries, onItemSelected });
  ReactDOM.render(element, logDiv);
}

function clearLog() {
  parsedEntries = [];
  applyConfig();
  renderLog();
}

// Open the settings page
function showSettings() {
  var page = document.getElementById('settingsPage');
  function onClose(changed) {
    if (changed) {
      //re-render
      applyConfig();
      parsedEntries.forEach(catalog);
      renderLog();
    }
    ReactDOM.unmountComponentAtNode(page);
  }
  var element = React.createElement(Settings, {onClose, storageKey: global.file});
  ReactDOM.render(element, page);
}
