var React = require('react');

module.exports = React.createClass({
  getDefaultProps: function () {
    return { values: [] };
  },
  changeValue: function (index, event) {
    var values = this.props.values.concat();
    values[index] = event.target.value;
    this.props.onChange(values);
  },
  onBlur: function (index) {
    var values = this.props.values.filter(Boolean);
    if (values.length < this.props.values.length) {
      this.props.onChange(values);
    }
  },
  render: function () {
    var self = this;
    var rows = this.props.values.concat([null]).map(function (value, index) {
      value = value === null ? '' : value;
      var readonly = index > 2;

      var input = readonly ? 'log order' : React.createElement('input', {
        placeholder: 'log order',
        type: 'text',
        id: 'text' + index,
        value: value,
        onChange: self.changeValue.bind(self, index),
        onBlur: self.onBlur.bind(self, index)
      });

      var label = React.createElement('label', { htmlFor: 'text' + index }, index === 0 ? 'Group by' : 'then by');

      return React.createElement('tr', {key: index}, [
        React.createElement('td', {
          style: {textAlign: 'right'},
          key: 'left'
        }, label),
        React.createElement('td', {key: 'right'}, input)
      ]);
    });

    return React.createElement('table', {key: 'table', className: 'groupby'}, [
      React.createElement('tbody', {key: 'tbody'}, rows)
    ]);
  }
});
