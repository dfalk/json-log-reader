'use strict';

var map = new WeakMap(), idCounter = 0;

var React = require('react');
var Infinite = require('react-infinite');
var Resizable = require('react-component-resizable');
var Immutable = require('immutable');
var classNames = require('classnames');
var levenshtein = require('fast-levenshtein');

function defaultKeyGetter(item) {
  if (map.has(item)) {
    return map.get(item);
  }
  var key = idCounter++;
  map.set(item, key);
  return key;
}

function defaultChildGetter(item) {
  return item.children;
}

function LevenshteinSubminimal(needle, haystack) {
  var needleLength = needle.length;
  var haystackLength = haystack.length;

  var f = function(s){return levenshtein.get(needle, s)};

  if(needleLength >= haystackLength) {
    return {distance: f(haystack), matchText: haystack};
  }

  var match = {distance: haystackLength + 2, matchText: null}, c1 = needleLength - 1, c2 = needleLength + 1;
  for(var p = 0; p < haystackLength - c1; p++) {
    for(var l = c1, c3 = Math.min(c2, haystackLength - p); l <= c3; l++) {
      var matchText = haystack.substr(p, l), distance = f(matchText);
      if(match.distance >= distance) {
        match = {distance: distance, matchText: matchText};
      }
    }
  }
  return match;
}

module.exports = React.createClass({
	getDefaultProps: function () {
		return {
      onItemSelected: () => {},
			keyGetter: defaultKeyGetter,
			childGetter: defaultChildGetter,
      fuzzyFilter: true,
			maxLevel: 10,
			itemHeight: 30,
      textboxHeight: 20
		};
	},
	getInitialState: function () {
		return { expandedKeys: Immutable.Set(), containerHeight: 200 };
	},
	containerResize(dimensions) {
		this.setState({ containerHeight: dimensions.height })
	},
	render: function () {
    let filter = () => true;
    if (this.state.filter) {
      var filterText = this.state.filter;
      if (this.props.fuzzyFilter) {
        filter =  label => LevenshteinSubminimal(filterText, label).distance <= 1;
      }
      else {
        var re = new RegExp(this.state.filter, 'i');
        filter = re.test.bind(re);
      }
    }
    const getRenderItems = (items, level, hidden, noFilter) => {
      let result = [], itemCount = 0, someFiltered = false;
      items.forEach(item => {
        let key = this.props.keyGetter(item);
        let childItems = (level < this.props.maxLevel) && this.props.childGetter(item);
        let collapsed = !!childItems && this.state.expandedKeys.has(key);
        let expanded = !!childItems && !collapsed;
        let selected = this.state.selectedKey === key;
        let descendantRenderItems, descendantCount = 0, someDescendantsWereFiltered;

        //Attempt to filter this out
        let matchesFilter = filter(item.label);

        if (childItems) {
          var r = getRenderItems(childItems, level + 1, hidden || collapsed, noFilter || matchesFilter);
          descendantRenderItems = r[0];
          descendantCount = r[1];
          someDescendantsWereFiltered = r[2];
        }
        else {
          if (!matchesFilter) {
            someFiltered = true;
          }
          else {
            itemCount++;
          }
        }

        itemCount += descendantCount;

        var props = {
          className: classNames('treeitem', 'level-' + level, { collapsed, expanded, hidden, selected }),
          style: {height: this.props.itemHeight + 'px'},
          key
        };
        if (descendantRenderItems) {
          props.onClick = () => {
            let action = collapsed ? 'delete' : 'add';
            this.setState({ expandedKeys: this.state.expandedKeys[action](key) });
          }
        }
        else {
          props.onClick = () => {
            this.setState({selectedKey: key});
            this.props.onItemSelected(item);
          }
        }

        if (descendantCount || matchesFilter) {
          result.push(React.createElement('div', props, [
            item.label, React.createElement('div', {
              className: classNames('childCount', {visible: collapsed}), key: 'childcount'
            }, descendantCount)
          ]));
        }
        if (descendantRenderItems)
          result.push(...descendantRenderItems);
      });
      return [result, itemCount, someFiltered];
    };

		return React.createElement(Resizable, Object.assign({
			style: { height: "100%" }
		}, this.props, {
			onResize: this.containerResize
		}), [
      React.createElement('input', {
        value: this.state.filter,
        onChange: event => this.setState({filter: event.target.value}),
        style: {height: this.props.textboxHeight + 'px'},
        placeholder: 'Filter',
        className: 'treeFilter'
      }),
      React.createElement(Infinite, {
				containerHeight: this.state.containerHeight - this.props.textboxHeight,
				elementHeight: this.props.itemHeight,
				key: 'infinite'
			}, getRenderItems(this.props.data, 0, false)[0])
		]);
	}
});
